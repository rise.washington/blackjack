const { Router } = require("express");
const { MongoClient, ObjectId } = require("mongodb");

if (process.env.MODE == "production") {
    url = `mongodb://2217135:${process.env.MONGO_PASSWORD}@192.168.171.67`;
} else {
    url = "mongodb://127.0.0.1:27017";
}  
console.log(url);

const mongoClient = new MongoClient(url);
const db = mongoClient.db(
    process.env.MODE === "production" ? "2217135" : "blackjack"
);

const apiRouter = Router();

apiRouter.post("/user/", async (req, res) => {
    try {
        const users = db.collection("gamblers");
        delete req.body.password;
        const newUser = await users.insertOne({
            ...req.body,
        });
        res.json(newUser);
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
});

apiRouter.get("/user/:id", async (req, res) => {
    try {
        const users = db.collection("gamblers");
        const searchedUser = await users.findOne({
            _id: new ObjectId(req.params.id),
        });
        res.json(searchedUser);
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
});

apiRouter.patch("/user/:id", async (req, res) => {
    try {
        const users = db.collection("gamblers");
        if (req.body.username) {
            const username = req.body.username;
            const updatedUser = await users.updateOne(
                {
                    _id: new ObjectId(req.params.id),
                },
                {
                    $set: { username } // { "username" : username }
                }
            );
            res.json(updatedUser);
        } else {
            res.sendStatus(500);
        }
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
});

apiRouter.delete("/user/:id", async (req, res) => {
    try {
        const users = db.collection("gamblers");
        const searchedUser = await users.deleteOne({
            _id: new ObjectId(req.params.id),
        });
        res.json(searchedUser);
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
});

module.exports = apiRouter; 