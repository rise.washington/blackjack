const MDCTextField = mdc.textField.MDCTextField;
const textFields = [].map.call(
    document.querySelectorAll(".mdc-text-field"),
    function (el) {
        return new MDCTextField(el);
    }
);

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
    document.querySelectorAll(".mdc-checkbox"),
    function (el) {
        return new MDCCheckbox(el);
    }
);

console.log(document.forms[0]);
const myForm = document.forms[0];
const singUpButton = myForm[8];
const [
    ageInput,
    birthDateInput,
    fullNameInput,
    usernameInput,
    enterPasswordInput,
    confirmPasswordInput,
    legalCheckbox,
    termsCheckbox
] = myForm;
console.dir(singUpButton);

function validateEligibility(event) {
    event.preventDefault();

    const ageValue = ageInput.value;
    const birthDateValue = birthDateInput.value;
    const fullNameValue = fullNameInput.value;
    const usernameValue = usernameInput.value;
    const enterPasswordValue = enterPasswordInput.value;
    const confirmPasswordValue = confirmPasswordInput.value;
    const legalValue = legalCheckbox.checked;
    const termsValue = termsCheckbox.checked;

    console.log(`Age: ${ageValue}`);
    console.log(`Birth Date: ${birthDateValue}`);
    console.log(`Full Name: ${fullNameValue}`);
    console.log(`Username: ${usernameValue}`);
    console.log(`Enter Password: ${enterPasswordValue}`);
    console.log(`Confirm Password: ${confirmPasswordValue}`);
    console.log(legalValue, termsValue);

    if(legalValue){
        console.log("The user has checked the legal checkbox");
    } else{
        console.log("The user has not checked the legal checkbox");
    }

    if(termsValue)
    {
       console.log("The user has checked the terms checkbox");
    } else{
        console.log("The user has not checked the terms checkbox");
    }
  
    const boxesChecked = legalValue && termsValue;
    const ofAge = ageValue >= 13;
    function isNotEmpty(str) {
        return str !== "";
    }

    const noEmptyFields =
    isNotEmpty(ageValue) &&
    isNotEmpty(birthDateValue) &&
    isNotEmpty(fullNameValue) &&
    isNotEmpty(usernameValue) &&
    isNotEmpty(enterPasswordValue) &&
    isNotEmpty(confirmPasswordValue);

    const passwordMatch = enterPasswordValue === confirmPasswordValue;
    if (passwordMatch && boxesChecked && ofAge && noEmptyFields) {
        console.log("The user is eligible");
    } else {
        console.log("The user is ineligible");
    }
}

singUpButton.addEventListener("click", validateEligibility);


